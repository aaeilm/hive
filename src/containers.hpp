// (c) Mel Aise 2018

#pragma once

#include <vector>
#include <string>
#include <forward_list>
#include <stdexcept>
#include <cassert>

namespace hive {

using namespace std;

// NOTE only works for ascii chars
template<typename T>
struct Trie {
    struct Node {
        T obj;
        char ch;
        vector<Node> children;

        bool has_children() {
            return children.size() > 0;
        }

        Node *get_child(char ch) {
            for (auto &n : children) {
                if (n.ch == ch) {
                    return &n;
                }
            }

            return nullptr;
        }
    };

    Node root;

    void insert(const string &str, T obj) {
        if (str.size() == 0) {
            throw runtime_error("cannot insert into trie with empty string");
        }

        Node *curr = &root;
        for (auto ch : str) {
            Node *child = curr->get_child(ch);

            if (child) {
                curr = child;
            } else {
                curr->children.emplace_back();
                curr = &curr->children.back();
                curr->ch = ch;
            }
        }

        // youll always have a node here
        //   unless you insert w/ an empty string, see above
        assert(curr);
        curr->obj = obj;
    }

    T at(const string &str) {
        Node *curr = &root;
        for (auto ch : str) {
            Node *child = curr->get_child(ch);

            if (child) {
                curr = child;
            } else {
                throw out_of_range("no object in trie at: " + str);
            }
        }

        return curr->obj;
    }
};

template<typename T>
struct Pool {
    Pool(auto sz)
    : Pool(sz, T()) {
    }

    Pool(auto sz, const T &init_to)
    : objs(vector<T>(sz))
    , init_to(init_to) {
        fill_pool();
    }

    T *get() {
        if (!lst.empty()) {
            T *ret = lst.front();
            lst.pop_front();
            new (ret) T(init_to);
            return ret;
        } else {
            return nullptr;
        }
    }

    void put_back(T *ptr) {
        if (ptr > (&objs[0] + objs.size() * sizeof(T))) {
            throw runtime_error("object not part of pool");
        }
        ptr->~T();
        lst.push_front(ptr);
    }

    void fill_pool() {
        lst.clear();
        const unsigned ct = objs.size();
        for (unsigned i = 0; i < ct; ++i) {
            lst.push_front(&objs[0] + i);
        }
    }

private:
    vector<T> objs;
    forward_list<T *> lst;
    const T init_to;
};

#if 0

#include <iostream>

struct Poolable {
    Poolable()
    : item(0) {
    }

    Poolable(auto item)
    : item(item) {
    }

    int get_item() {
        return item;
    }

private:
    int item;
};

// TODO for some reason this gives some type of memory errors
struct Allocs {
    Allocs()
    : wawa(new int) {
        *wawa = 0;
    }

    ~Allocs() {
        delete wawa;
    }

    Allocs(const Allocs &other)
    : wawa(new int) {
        memcpy(this->wawa, other.wawa, sizeof(int));
    }

    Allocs &operator= (const Allocs &other) {
        if (this != &other) {
            memcpy(this->wawa, other.wawa, sizeof(int));
        }
        return *this;
    }

private:
    int *wawa;
};

void new_pool_test() {
    auto p1 = Pool<Poolable>(500);
    auto p11 = p1.get();
    cout << p11->get_item() << endl;

    auto p2 = Pool<Poolable>(5, Poolable(5));
    auto p21 = p2.get();
    cout << p21->get_item() << endl;
    auto p22 = p2.get();
    auto p23 = p2.get();
    auto p24 = p2.get();
    auto p25 = p2.get();

    auto p26 = p2.get();
    cout << "p26 is " << (p26 ? "not null" : "null") << endl;

    p1.put_back(p11);

    try {
        p1.put_back(p22);
    } catch (const runtime_error &e) {
        cout << "p1 << p22 " << e.what() << endl;
    }

    {
        auto a = Allocs();
        cout << "k" << endl;
    }

    auto a1 = Pool<Allocs>(10);
    auto a11 = a1.get();
    a1.put_back(a11);
}

void new_trie_test() {
    Trie<int *> t;

    int i = 0, j = 1, k = 2, l = 3;
    t.insert("hello", &i);
    t.insert("heck", &j);
    t.insert("hell", &k);
    t.insert("ho+i", &l);
    t.insert("hop", &k);

    cout << boolalpha;
    cout << (t.at("hello") == &i) << endl;
    cout << (t.at("heck") == &j) << endl;
    cout << (t.at("hell") == &k) << endl;

    cout << (t.at("hop") ==  &k) << endl;
    cout << (t.at("hoi") ==  &l) << endl;
    cout << (t.at("hoooi") ==  &l) << endl;
    cout << noboolalpha;

    cout << (t.at("hooop") ==  &k) << endl;

    try {
        t.at("f");
    } catch (const out_of_range &e) {
        cout << e.what() << endl;
    }
}

#endif

}
