// (c) Mel Aise 2018

#include <GL/glew.h>

#include <regu/math.hpp>

#include "gfx.hpp"

namespace rgfx = regu::GFX;

namespace hive {

namespace GFX {
    rgfx::Context2d *ctx;
    Background *bg;

    void key_callback(GLFWwindow *win, int key, int scancode, int action, int mods) {
        (void) mods, (void)scancode;

        if (action == GLFW_PRESS) {
            switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(win, GLFW_TRUE);
                break;
            }
        }
    }

    void init() {
        rgfx::init(600, 600);
        glfwSetKeyCallback(rgfx::win, key_callback);

        ctx = new rgfx::Context2d();
        ctx->init();

        bg = new Background();

        bg->bgs.emplace_back();
        bg->bgs.back().add_file("content/test_bgs/a1.png");
        bg->bgs.back().add_file("content/test_bgs/a2.png");
        bg->bgs.back().add_file("content/test_bgs/a3.png");
        bg->bgs.back().speed = 0.15;

        bg->bgs.emplace_back();
        bg->bgs.back().add_file("content/test_bgs/b1.png");
        bg->bgs.back().add_file("content/test_bgs/b2.png");
        bg->bgs.back().speed = 0.25;

        bg->bgs.emplace_back();
        bg->bgs.back().add_file("content/test_bgs/a1.png");
        bg->bgs.back().add_file("content/test_bgs/a2.png");
        bg->bgs.back().add_file("content/test_bgs/a3.png");
        bg->bgs.back().speed = 0.35;

        bg->bgs.emplace_back();
        bg->bgs.back().add_file("content/test_bgs/b1.png");
        bg->bgs.back().add_file("content/test_bgs/b2.png");
        bg->bgs.back().speed = 0.45;

        bg->bgs.emplace_back();
        bg->bgs.back().add_file("content/test_bgs/a1.png");
        bg->bgs.back().add_file("content/test_bgs/a2.png");
        bg->bgs.back().add_file("content/test_bgs/a3.png");
        bg->bgs.back().speed = 0.55;
    }

    void deinit() {
        delete bg;

        ctx->deinit();
        delete ctx;
        rgfx::deinit();
    }

    void do_frame() {
        static double last, now;

        now = glfwGetTime();
        double dt = now - last;

        glfwPollEvents();
        glClear(GL_COLOR_BUFFER_BIT);
        glClearColor(0, 0, 1, 1);

        ctx->engage();
        bg->draw(dt);

        glfwSwapBuffers(rgfx::win);

        last = now;
    }

    bool should_close() {
        return glfwWindowShouldClose(rgfx::win);
    }

    //

    GLuint copy_texture(GLuint in) {
        // TODO sortof cheating
        //      bsides the mipmaps thing as long as the textures are all made the
        //      same way, we're good

        glBindTexture(GL_TEXTURE_2D, in);
        int w, h;
        rgfx::texture_get_dimensions(GL_TEXTURE_2D, 0, &w, &h);
        GLuint ret = rgfx::alloc_texture(nullptr, w, h);

        glCopyImageSubData(in, GL_TEXTURE_2D, 0, 0, 0, 0
            , ret, GL_TEXTURE_2D, 0, 0, 0, 0, w, h, 1);

        glBindTexture(GL_TEXTURE_2D, 0);

        return ret;
    }

    Animation::~Animation() {
        // TODO could probably pass the whole vector data
        for (auto tx : frames) {
            glDeleteTextures(1, &tx);
        }
    }

    Animation::Animation(const Animation &other)
    : frames(other.frames)
    , speed(other.speed) {
        for (auto iter = frames.begin(); iter != frames.end(); ++iter) {
            *iter = copy_texture(*iter);
        }
    }

    void Animation::add_file(const string &filepath) {
        try {
            int w, h;
            auto img = rgfx::load_image(filepath, &w, &h);
            frames.push_back(rgfx::alloc_texture(img, w, h));
            rgfx::free_image(img);
        } catch (...) {
            throw;
        }
    }

    void Animation::update(double dt) {
        time_at += dt;
        if (time_at >= speed) {
            time_at = 0;
            idx_at += 1;
            if (idx_at >= frames.size()) {
                idx_at = 0;
            }
        }
    }

    //

    void Background::draw(double dt) {
        float half_radius = regu::Math::hypotenuse(rgfx::window_width, rgfx::window_height) / 2.0f;
        float depth = 0.15;

        double mx, my;
        glfwGetCursorPos(rgfx::win, &mx, &my);

        auto center = glm::vec2((float)rgfx::window_width, (float)rgfx::window_height) / 2.0f;
        auto m_offset = glm::vec2(mx, my) - center;

        float m_distance = length(m_offset);

        float ratio = glm::clamp(m_distance / half_radius, 0.0f, 1.0f);

        ratio = sin(ratio * M_PI / 2);

        for (auto &a : bgs) {
            a.update(dt);
        }

        ctx->engage();
        ctx->set_color(1,1,1,1);
        const unsigned ct = bgs.size();
        for (unsigned i = 0; i < ct; ++i) {
            auto temp = normalize(m_offset)
                      * (1 - (float)i/(ct - 1))
                      * half_radius * ratio * depth
                      * -1.0f
                      + center;

            float down_scl = 1 - ((1-(float)i/(ct - 1)) * depth);

            auto &a = bgs[i];
            ctx->draw(a.frames[a.idx_at], temp.x-250*down_scl, temp.y-250*down_scl, 0, down_scl, down_scl);
        }
    }
}

}
