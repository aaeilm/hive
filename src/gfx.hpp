// (c) Mel Aise 2018

#pragma once

#include <vector>

#include <regu/gfx.hpp>

namespace hive {

using namespace std;

namespace GFX {
    void init();
    void deinit();
    void do_frame();
    bool should_close();

    struct Animation {
        vector<GLuint> frames;
        double speed;

        // TODO
        Animation() { }
        ~Animation();
        Animation(const Animation &);

        // TODO add texture as to not reload images
        //      freeing textures needs 2 be  hanldled diffrently
        void add_file(const string &);
        void update(double dt);

        float time_at = 0;
        unsigned idx_at = 0;
    };

    struct Background {
        vector<Animation> bgs;

        void draw(double dt);
    };
}

}
