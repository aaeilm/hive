// (c) Mel Aise 2018

#include <iostream>
#include <stdexcept>

#include <unistd.h>

#include "parser.hpp"
#include "gfx.hpp"

using namespace hive;

int main(void) {
    try {
        GFX::init();
    } catch (const exception &e) {
        cout << e.what() << endl;
    }

    while (!GFX::should_close()) {
        GFX::do_frame();
        usleep(30);
    }

    GFX::deinit();

    return 0;
}
