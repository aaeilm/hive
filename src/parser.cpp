// (c) Mel Aise 2018

#include <iostream>

#include "containers.hpp"

#include "parser.hpp"

namespace hive {

namespace Parser {
    Trie<WordType> trie;

    struct Stretcher {
        Trie<WordType>::Node *curr;
        Atom atom;
        bool try_ish;

        void feed(char ch);
        void reset();
        bool is_finished();
    };

    void Stretcher::feed(char ch) {
        // for error reporting
        char buf[2] = { 0 };

        if (ch == '+') {
            // TODO check for this before it gets here
            throw runtime_error("\'+\' in stretcher stream");
        }

        if (ch >= 'A' && ch <= 'Z') {
            atom.caps_ct++;
            ch += 0x20;
        }

        static unsigned ish_at;

        if (try_ish) {
            static const char *ISH = "ish";
            if (ch != ISH[ish_at]) {
                buf[0] = ch;
                throw out_of_range(buf);
            }

            ish_at++;
            if (ish_at > 2) {
                atom.ish_ct++;
                ish_at = 0;
            }

            return;
        }

        auto *child = curr->get_child(ch);
        if (child) {
            curr = child;
        } else {
            auto *plus = curr->get_child('+');

            // TODO check if type of plus is end of word

            if (plus) {
                if (ch == curr->ch) {
                    atom.stretch_ct++;
                } else {
                    // the next letter is not a repeat
                    //   maybe youre continuing in the word?
                    auto *grandchild = plus->get_child(ch);
                    if (grandchild) {
                        curr = grandchild;
                    } else {
                        // maybe youre on the end of a word
                        //   , and the next char starts some ishes
                        // otherwise this isnt a valid word
                        if (curr->obj != NOT_A_WORD && ch == 'i') {
                            try_ish = true;
                            ish_at = 1;
                        } else {
                            buf[0] = ch;
                            throw out_of_range(buf);
                        }
                    }
                }
            } else {
                // maybe youre on the end of a word
                //   , and the next char starts some ishes
                // otherwise this isnt a valid word
                if (curr->obj != NOT_A_WORD && ch == 'i') {
                    try_ish = true;
                    ish_at = 1;
                } else {
                    buf[0] = ch;
                    throw out_of_range(buf);
                }
            }
        }

        atom.word_type = curr->obj;
    }

    void Stretcher::reset() {
        atom = Atom();
        atom.type = Atom::STRETCHY;
        curr = &trie.root;
        try_ish = false;
    }

    bool Stretcher::is_finished() {
        return atom.word_type != NOT_A_WORD;
    }

    //

    Stretcher stretcher;

    const pair<string, WordType> pairs[] = {
        make_pair("hell+o", HELLO) ,
        make_pair("wo+w", WOW) ,
    };

    void init() {
        for (const auto &p : pairs) {
            // TODO to lowercase
            trie.insert(p.first, p.second);
        }
    }

    enum State {
        NONE = 0,
        COMMENT ,
        STRETCHY ,
        STRING ,
    };

    bool is_whitespace(char ch) {
        return ch == '\n' || ch == ' ' || ch == '\t' || ch == '\r' || ch == ',' || ch == ':';
    }

    bool is_stretchy(char ch) {
        return !is_whitespace(ch) && ch != '(' && ch != ')' && ch != '"' && ch != '\0';
    }

    vector<Group> parse(const string &str) {
        stretcher.reset();

        unsigned paren_ct = 0;

        vector<Group> groups;
        Group *curr_group;

        vector<Warning> warnings;

        string filename_buf;
        filename_buf.reserve(256);

        State state = NONE;
        State state_precomment = NONE;
        unsigned line_at = 1;

        // TODO just use #defines?

        auto finish_stretchy = [&] {
            if (stretcher.is_finished()) {
                if (curr_group) {
                    curr_group->atoms.emplace_back(stretcher.atom);
                }
            } else {
                warnings.emplace_back(line_at, "bad stretch word");
            }
            stretcher.reset();
        };

        auto finish_string = [&] {
            if (curr_group) {
                curr_group->atoms.emplace_back();
                Atom &a = curr_group->atoms.back();
                a.type = Atom::STRING;
                a.str = filename_buf;
            }
        };

        for (auto iter = str.begin(); iter != str.end(); ++iter) {
            const char ch = *iter;

            if (ch == '(') {
                if (state != COMMENT) {
                    state_precomment = state;
                    state = COMMENT;
                }

                paren_ct++;
            } else if (ch == ')') {
                if (state == COMMENT) {
                    paren_ct--;
                    if (!paren_ct) {
                        state = state_precomment;
                    }
                } else {
                    warnings.emplace_back(line_at, "unmatched paren");
                }
            } else if (ch == '"') {
                if (state == STRETCHY) {
                    finish_stretchy();
                    state = NONE;
                }

                switch (state) {
                case COMMENT:
                    break;
                case NONE:
                    filename_buf.clear();
                    state = STRING;
                    break;
                case STRING:
                    finish_string();
                    state = NONE;
                    break;
                default:
                    assert(false);
                    break;
                }
            } else if (ch == '!') {
                if (state == STRETCHY) {
                    finish_stretchy();
                    state = NONE;
                }

                switch (state) {
                case COMMENT:
                case STRING:
                    break;
                case NONE: {
                    auto peek = iter + 1;
                    if (peek == str.end()) {
                        warnings.emplace_back(line_at, "incomplete group type");
                        continue;
                    }

                    groups.emplace_back();
                    curr_group = &groups.back();
                    switch (*peek) {
                    case 'm': case 'M':
                        curr_group->type = Group::Type::MAIN;
                        break;
                    case 'i': case 'I':
                        curr_group->type = Group::Type::IMAGES;
                        // TODO get image number
                        break;
                    case 's': case 'S':
                        curr_group->type = Group::Type::SOUNDS;
                        break;
                    default:
                        warnings.emplace_back(line_at, "invalid group type: " + *peek);
                        break;
                    }

                    iter++;
                } break;
                default:
                    assert(false);
                    break;
                }
            } else if (is_whitespace(ch)) {
                if (ch == '\n') {
                    if (state == STRING) {
                        warnings.emplace_back(line_at, "line break in filename");
                        finish_string();
                        state = NONE;
                    }
                }

                switch (state) {
                case COMMENT:
                case NONE:
                    break;
                case STRETCHY:
                    finish_stretchy();
                    state = NONE;
                    break;
                case STRING:
                    filename_buf += ch;
                    break;
                default:
                    assert(false);
                    break;
                }

                // down here for proper line_at in warnings
                if (ch == '\n') {
                    line_at++;
                }
            } else {
                if (state == NONE) {
                    state = STRETCHY;
                }

                switch (state) {
                case STRETCHY:
                    try {
                        stretcher.feed(ch);
                    } catch (const out_of_range &e) {
                        warnings.emplace_back(line_at, "parse error at char: " + string(e.what()));

                        while (is_stretchy(*iter)) {
                            iter++;
                        }
                        iter--;
                        stretcher.reset();
                        state = NONE;
                    }
                    break;
                case STRING:
                    filename_buf += ch;
                    break;
                case COMMENT:
                    break;
                default:
                    assert(false);
                    break;
                }
            }
        }

        switch (state) {
        case NONE:
            break;
        case STRETCHY:
            finish_stretchy();
            break;
        case COMMENT:
            warnings.emplace_back(line_at, "unmatched paren");
            // TODO check state precomment
            break;
        case STRING:
            warnings.emplace_back(line_at, "unfinished filename");
            finish_string();
            break;
        default:
            assert(false);
            break;
        }

        for (const auto &warn : warnings) {
            cout << warn << endl;
        }

        return groups;
    }
}

}
