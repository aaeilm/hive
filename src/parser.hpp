// (c) Mel Aise 2018

#pragma once

#include <string>
#include <vector>
#include <iostream>

namespace hive {

using namespace std;

namespace Parser {
    enum WordType {
        NOT_A_WORD = 0,
        HELLO ,
        WOW ,
    };

    struct Atom {
        enum Type {
            STRING ,   // filenames
            STRETCHY ,
        } type;

        // TODO look into boost::any
        string str;
        struct {
            WordType word_type;
            unsigned stretch_ct;
            unsigned ish_ct;
            unsigned caps_ct;
        };

        friend ostream &operator<< (ostream &o, const Parser::Atom &a) {
            if (a.type == Atom::STRETCHY) {
                return o << "Atom<"
                         << a.word_type  << " "
                         << a.stretch_ct << " "
                         << a.ish_ct     << " "
                         << a.caps_ct    << ">";
            } else {
                return o << "Atom<"
                         << a.str << ">";
            }
        }
    };

    struct Group {
        enum Type {
            SOUNDS ,
            IMAGES ,
            MAIN ,
        } type;

        unsigned image_at;

        vector<Atom> atoms;
    };

    struct Warning {
        Warning(unsigned line_at, const string &str)
        : line_at(line_at)
        , str(str) {
        }

        const char *what() {
            return str.c_str();
        }

        unsigned line() {
            return line_at;
        }

        friend ostream &operator<< (ostream &o, const Parser::Warning &w) {
            return o << "Warning<line: " << w.line_at << " : " << w.str << ">";
        }

    private:
        const unsigned line_at;
        const string str;
    };

    void init();
    vector<Group> parse(const string &str);
}

}
